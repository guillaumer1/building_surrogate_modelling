# Bayesian learning for uncertainty-aware building energy surrogate models.

<!-- #region -->
Bayesian neural networks have been applied to quantify uncertainty of automated machine learning predictions, where the uncertainty is broken down into data-caused uncertainty (aleatoric uncertainty) and model-caused uncertainty (epistemic). In the realm of surrogate modelling, where the training data originates from deterministic (besides numerical noise) simulation engines, we are mosty concerned with the uncertainty introduced by the surrogate model itself.

In this study, we acknowledge that approximative surrogate models introduce uncertainty into the performance prediction step, and use Bayesian learning models to quantify that uncertainty. We quantify how the Bayesian modelling approach impairs overall surrogate model emulation accuracy, and how good the quality of the uncertainty estimates is.

We propose, that Bayesian models can be crucial for hybridizing/linking slow, white-box engineering models and fast, black-box surrogate models, which is illustrated in the Figure below.


![Alt text](./Pics/Graphical_Abstract.png?raw=true "Title")
<!-- #endregion -->


## Some requirements
We recommend to install the most recent version of the [Anaconda distribution](https://www.anaconda.com/) plus the most recent versions of the following packages:
GPy: https://github.com/SheffieldML/GPy
Keras/Tensorflow:https://keras.io/

As we provide a dataset for training the neural network as presented for the [NetZero navigator project](http://enerarxiv.org/page/thesis.html?id=1975), no further installation of building simulation tools is required. If you are interested in Bayesian approaches to train you own building energy surrogate model, we recommend to use the [BESOS platform](https://besos.uvic.ca/). 

## Related publication:
Westermann, Paul and Ralph Evins. “Using Bayesian deep learning approaches for uncertainty-aware building energy surrogate models.” (2020). 
[https://arxiv.org/abs/2010.03029](https://arxiv.org/abs/2010.03029)



## Contact:

Paul Westermann, Ralph Evins [Energy in Cities group, University of Victoria, Canada](https://energyincities.gitlab.io/website/)

<img src="../location_independent_surrogates/Pics/uvic_logo.jpg" alt="drawing" width="200"/>

#### Acknowledgements:
Thanks to the BESOS development team for maintaing the [BESOS platform](https://besos.uvic.ca/) (where we ran the majority of our experiments).

```python

```
