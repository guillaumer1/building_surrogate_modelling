# Codes based on Yarin Gal's work on dropout neural networks https://github.com/yaringal/DropoutUncertaintyExps/tree/master/net
# This code is based on the code by Jose Miguel Hernandez-Lobato used for his 
# paper "Probabilistic Backpropagation for Scalable Learning of Bayesian Neural Networks".

import warnings
warnings.filterwarnings("ignore")

import math
from scipy.special import logsumexp
import numpy as np

from tensorflow.keras.regularizers import l2
from tensorflow.keras import Input
from tensorflow.keras.layers import Dropout, Dense, LeakyReLU, ReLU
from tensorflow.keras import Model
from sklearn.metrics import r2_score
from tensorflow.keras.wrappers.scikit_learn import KerasRegressor
from sklearn.model_selection import GridSearchCV
import time


class net:

    def __init__(self, X_train, y_train, n_hidden, n_epochs = 40,
        normalize = False, tau = 1.0, dropout = 0.05):

        """
            Constructor for the class implementing a Bayesian dropout 
            neural network trained with the probabilistic back propagation method.

            @param X_train      Matrix with the features for the training data.
            @param y_train      Vector with the target variables for the
                                training data.
            @param n_hidden     Vector with the number of neurons for each
                                hidden layer.
            @param n_epochs     Numer of epochs for which to train the
                                network. The recommended value 40 should be
                                enough.
            @param normalize    Whether to normalize the input features. This
                                is recommended unles the input vector is for
                                example formed by binary features (a
                                fingerprint). In that case we do not recommend
                                to normalize the features.
            @param tau          Tau value used for regularization
            @param dropout      Dropout rate for all the dropout layers in the
                                network.
        """

        # We normalize the training data to have zero mean and unit standard
        # deviation in the training set if necessary

        if normalize:
            self.std_X_train = np.std(X_train, 0)
            self.std_X_train[ self.std_X_train == 0 ] = 1
            self.mean_X_train = np.mean(X_train, 0)
        else:
            self.std_X_train = np.ones(X_train.shape[ 1 ])
            self.mean_X_train = np.zeros(X_train.shape[ 1 ])

        X_train = (X_train - np.full(X_train.shape, self.mean_X_train)) / \
            np.full(X_train.shape, self.std_X_train)

        y_train_normalized = np.array(y_train, ndmin = 2).T
        
        # We construct the network
        N = X_train.shape[0]
        

        def estimator_1(lengthscale = .1, **kwargs):
            # Define the regularizer
            reg = lengthscale**2 * (1 - dropout) / (2. * N * tau)
            
            inputs = Input(shape=(X_train.shape[1],))
            
            # Add the hidden layers:
            inter = Dense(n_hidden[0], kernel_regularizer=l2(reg))(inputs)
            inter = LeakyReLU()(inter)
            for i in range(len(n_hidden) - 1):
                inter = Dropout(dropout)(inter, training=True)
                inter = Dense(n_hidden[i+1], kernel_regularizer=l2(reg))(inter)
                inter = LeakyReLU()(inter)
            inter = Dropout(dropout)(inter, training=True)
            
            
            outputs = Dense(y_train_normalized.shape[1], kernel_regularizer=l2(reg))(inter)
            outputs = LeakyReLU()(outputs)
            model = Model(inputs, outputs)
            model.compile(loss='mean_squared_error', optimizer='adam')
            return model
        
        # We iterate the learning process
        start_time = time.time()
             
        
        # Use the following codes to run a grid search:
        #clf = KerasRegressor(build_fn=estimator_1, verbose=True, epochs=n_epochs, batch_size=128)
        #hyperparameters = {
        #    'lengthscale': [1e-3, 1e-2, 0.1,],
        #}
        #search = GridSearchCV(clf, param_grid = hyperparameters, iid=True, cv=3, n_jobs=5, scoring = 'r2', verbose=True)  
        #search.fit(X_train, y_train_normalized, epochs=n_epochs, verbose=0)
        #self.model = search.best_estimator_
        #print(search.cv_results_)
        #print(search.best_params_)
        
        
        # Use the following codes to run an individual training run:
        self.model = estimator_1()
        self.model.fit(X_train, y_train_normalized, epochs=n_epochs, batch_size=128)
        self.tau = tau
        self.running_time = time.time() - start_time

        
        # We are done!
        
    def fit(self, X_train, y_train, n_epochs):
        #X_train = (X_train - np.full(X_train.shape, self.mean_X_train)) / np.full(X_train.shape, self.std_X_train)
        
        #y_train_normalized = (y_train - self.mean_y_train) / self.std_y_train
        #y_train_normalized = np.array(y_train_normalized, ndmin = 2).T
        #print(y_train_normalized)
        self.model.fit(X_train, y_train_normalized, epochs=n_epochs)

    def predict(self, X_test, y_test=None, return_std = False):

        """
            Function for making predictions with the Bayesian neural network.

            @param X_test   The matrix of features for the test data
            
    
            @return m       The predictive mean for the test target variables.
            @return v       The predictive variance for the test target
                            variables.
            @return v_noise The estimated variance for the additive noise.

        """

        X_test = np.array(X_test, ndmin = 2)
        if y_test is not None:
            y_test = np.array(y_test, ndmin = 2).T

        # We normalize the test set

        X_test = (X_test - np.full(X_test.shape, self.mean_X_train)) / \
            np.full(X_test.shape, self.std_X_train)

        # We compute the predictive mean and variance for the target variables
        # of the test data

        model = self.model
        standard_pred = model.predict(X_test, batch_size=500, verbose=1)
        standard_pred = standard_pred * self.std_y_train + self.mean_y_train
        if y_test is not None:
            rmse_standard_pred = np.mean((y_test.squeeze() - standard_pred.squeeze())**2.)**0.5

        T = 1000 # original value: 10'000
        
        Yt_hat = np.array([model.predict(X_test, batch_size=500, verbose=0) for _ in range(T)])
        Yt_hat = Yt_hat * self.std_y_train + self.mean_y_train

        MC_pred = np.mean(Yt_hat, 0)
        MC_uncertainty = np.var(Yt_hat, 0)
        if y_test is not None:
            rmse = np.mean((y_test.squeeze() - MC_pred.squeeze())**2.)**0.5

        # We compute the test log-likelihood
        if y_test is not None:
            ll = (logsumexp(-0.5 * self.tau * (y_test[None] - Yt_hat)**2., 0) - np.log(T) 
                - 0.5*np.log(2*np.pi) + 0.5*np.log(self.tau))
            test_ll = np.mean(ll)

        # We are done!
        if return_std==True:
            res = (MC_pred, MC_uncertainty)
        else:
            res = MC_pred
        
        return  res #, rmse_standard_pred, rmse, test_ll
    
    def score(self, X_test, y_test):
        
        return r2_score(y_test, self.predict(X_test))
