 ! The following Location and Design Day data are produced as possible from the weather source data.
 ! Wind Speeds follow the traditional values (6.7 m/s heating, 3.35 m/s cooling)
 ! No special attempts at re-creating or determining missing data parts (e.g. Wind speed or direction)
 ! are done.  Therefore, you should look at the data and fill in any incorrect values as you desire.
 
 ! SizingPeriod:DesignDay and other objects are formatted to meet criteria of EnergyPlus V7.1 and later
 
 ! Some users have indicated that they would like the actual day of week from the weather file to be
 ! used as the start day of week for a RunPeriod even though this is relatively meaningless for
 ! TMY data files as future months will not use their start days of week (simulations need to show
 ! continuity in day of week presentation).
 
 ! In an effort to accomodate, the following initial month days are included:
 ! Note JAN 1, 2016 is a Friday
 ! Note FEB 1, 2015 is a Sunday
 ! Note MAR 1, 2016 is a Tuesday
 ! Note APR 1, 2017 is a Saturday
 ! Note MAY 1, 2015 is a Friday
 ! Note JUN 1, 2015 is a Monday
 ! Note JUL 1, 2017 is a Saturday
 ! Note AUG 1, 2016 is a Monday
 ! Note SEP 1, 2014 is a Monday
 ! Note OCT 1, 2015 is a Thursday
 ! Note NOV 1, 2013 is a Friday
 ! Note DEC 1, 2013 is a Sunday
 
 ! Since the RunPeriod object changes in V9.0, the following are RunPeriod objects in comments:
 ! Version before EnergyPlus release V9.0
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !Friday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes;            !- Use Weather File Snow Indicators
 
 ! Version EnergyPlus release V9.0+
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !   ,             !- Begin Year
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !   ,             !- End Year
 !Friday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes,            !- Use Weather File Snow Indicators
 ! ;               !- Treat Weather as Acual
 
 Site:Location,
  Grono_GR_CHE Design_Conditions,     !- Location Name
      46.26,     !- Latitude {N+ S-}
       9.16,     !- Longitude {W- E+}
       1.00,     !- Time Zone Relative to GMT {GMT+/-}
     324.30;     !- Elevation {m}
  
 ! The following Sizing Period objects for Extreme and Typical conditions are calculated
 ! from the extreme (if any) and typical conditions on the weather source data.
 ! The actual weeks that will be used will exist on the weather file.
  
 SizingPeriod:WeatherFileConditionType,
   Summer Extreme,
   SummerExtreme,
   SummerDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Summer Typical,
   SummerTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Extreme,
   WinterExtreme,
   WinterDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Typical,
   WinterTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Autumn Typical,
   AutumnTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Spring Typical,
   SpringTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 


 ! Site:Precipitation. RoofIrrigation and Schedule:File objects for rainfall
 ! Data is built from rainfall data on the source data
  Site:Precipitation,  
    ScheduleAndDesignLevel,  !- Precipitation Model Type
    1.274,  !- Design Level for Total Annual Preciptation
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    1.274;  !- Average Total Annual Precipitation
 
  RoofIrrigation,  
    Schedule,  !- Irridation Model Type
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    ;  !- Irrigation Maximum Saturation Threshold (default used)
 
  Schedule:File, 
    Rainfall Data from EPW file,  !- Name
    ,                       !- Schedule Type Limits Name
    CHE_GR_Grono.067580_TMYx.2004-2018.rain,    !- File Name
    1,                       !- Column Number
    1,                       !- Rows to Skip at Top
    8760;    !- Number of Hours of Data


 ! No Design Conditions found for this Location
 ! Following definitions are created from weather source data
 ! Grono_GR_CHE Annual Heating 99.6%, MaxDB=-6.0C
 SizingPeriod:DesignDay,
  Grono Ann Htg 99.6% Condns DB,     !- Name
          1,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
       -6.0,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
       -6.0,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     97489.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Grono_GR_CHE Annual Heating 99%, MaxDB=-5.6C
 SizingPeriod:DesignDay,
  Grono Ann Htg 99% Condns DB,     !- Name
          1,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
       -5.6,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
       -5.6,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     97489.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Grono_GR_CHE Annual Cooling (DP=>MDB) .4%, MDB=30.8C DP=21.1C
 SizingPeriod:DesignDay,
  Grono Ann Clg .4% Condns DP=>MDB,     !- Name
          6,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       30.8,      !- Maximum Dry-Bulb Temperature {C}
       12.3,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       21.1,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     97489.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Grono_GR_CHE Annual Cooling (DP=>MDB) 1%, MDB=29.8C DP=20.7C
 SizingPeriod:DesignDay,
  Grono Ann Clg 1% Condns DP=>MDB,     !- Name
          6,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       29.8,      !- Maximum Dry-Bulb Temperature {C}
       12.3,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       20.7,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     97489.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Grono_GR_CHE Annual Cooling (DP=>MDB) 2%, MDB=29.1C DP=20.0C
 SizingPeriod:DesignDay,
  Grono Ann Clg 2% Condns DP=>MDB,     !- Name
          6,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       29.1,      !- Maximum Dry-Bulb Temperature {C}
       12.3,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       20.0,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     97489.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
