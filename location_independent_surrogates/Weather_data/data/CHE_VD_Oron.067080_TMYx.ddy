 ! The following Location and Design Day data are produced as possible from the weather source data.
 ! Wind Speeds follow the traditional values (6.7 m/s heating, 3.35 m/s cooling)
 ! No special attempts at re-creating or determining missing data parts (e.g. Wind speed or direction)
 ! are done.  Therefore, you should look at the data and fill in any incorrect values as you desire.
 
 ! SizingPeriod:DesignDay and other objects are formatted to meet criteria of EnergyPlus V7.1 and later
 
 ! Some users have indicated that they would like the actual day of week from the weather file to be
 ! used as the start day of week for a RunPeriod even though this is relatively meaningless for
 ! TMY data files as future months will not use their start days of week (simulations need to show
 ! continuity in day of week presentation).
 
 ! In an effort to accomodate, the following initial month days are included:
 ! Note JAN 1, 2015 is a Thursday
 ! Note FEB 1, 2018 is a Thursday
 ! Note MAR 1, 2015 is a Sunday
 ! Note APR 1, 2019 is a Monday
 ! Note MAY 1, 2016 is a Sunday
 ! Note JUN 1, 2018 is a Friday
 ! Note JUL 1, 2016 is a Friday
 ! Note AUG 1, 2019 is a Thursday
 ! Note SEP 1, 2014 is a Monday
 ! Note OCT 1, 2012 is a Monday
 ! Note NOV 1, 2016 is a Tuesday
 ! Note DEC 1, 2013 is a Sunday
 
 ! Since the RunPeriod object changes in V9.0, the following are RunPeriod objects in comments:
 ! Version before EnergyPlus release V9.0
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !Thursday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes;            !- Use Weather File Snow Indicators
 
 ! Version EnergyPlus release V9.0+
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !   ,             !- Begin Year
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !   ,             !- End Year
 !Thursday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes,            !- Use Weather File Snow Indicators
 ! ;               !- Treat Weather as Acual
 
 Site:Location,
  Oron_VD_CHE Design_Conditions,     !- Location Name
      46.57,     !- Latitude {N+ S-}
       6.86,     !- Longitude {W- E+}
       1.00,     !- Time Zone Relative to GMT {GMT+/-}
     828.80;     !- Elevation {m}
  
 ! The following Sizing Period objects for Extreme and Typical conditions are calculated
 ! from the extreme (if any) and typical conditions on the weather source data.
 ! The actual weeks that will be used will exist on the weather file.
  
 SizingPeriod:WeatherFileConditionType,
   Summer Extreme,
   SummerExtreme,
   SummerDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Summer Typical,
   SummerTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Extreme,
   WinterExtreme,
   WinterDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Typical,
   WinterTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Autumn Typical,
   AutumnTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Spring Typical,
   SpringTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 


 ! Site:Precipitation. RoofIrrigation and Schedule:File objects for rainfall
 ! Data is built from rainfall data on the source data
  Site:Precipitation,  
    ScheduleAndDesignLevel,  !- Precipitation Model Type
    1.231,  !- Design Level for Total Annual Preciptation
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    1.231;  !- Average Total Annual Precipitation
 
  RoofIrrigation,  
    Schedule,  !- Irridation Model Type
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    ;  !- Irrigation Maximum Saturation Threshold (default used)
 
  Schedule:File, 
    Rainfall Data from EPW file,  !- Name
    ,                       !- Schedule Type Limits Name
    CHE_VD_Oron.067080_TMYx.rain,    !- File Name
    1,                       !- Column Number
    1,                       !- Rows to Skip at Top
    8760;    !- Number of Hours of Data


 ! No Design Conditions found for this Location
 ! Following definitions are created from weather source data
 ! Oron_VD_CHE Annual Heating 99.6%, MaxDB=-13.8C
 SizingPeriod:DesignDay,
  Oron Ann Htg 99.6% Condns DB,     !- Name
          2,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
      -13.8,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
      -13.8,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     91757.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Oron_VD_CHE Annual Heating 99%, MaxDB=-12.6C
 SizingPeriod:DesignDay,
  Oron Ann Htg 99% Condns DB,     !- Name
          2,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
      -12.6,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
      -12.6,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     91757.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Oron_VD_CHE Annual Cooling (DP=>MDB) .4%, MDB=27.3C DP=17.5C
 SizingPeriod:DesignDay,
  Oron Ann Clg .4% Condns DP=>MDB,     !- Name
          7,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       27.3,      !- Maximum Dry-Bulb Temperature {C}
        8.4,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       17.5,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     91757.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Oron_VD_CHE Annual Cooling (DP=>MDB) 1%, MDB=26.6C DP=16.8C
 SizingPeriod:DesignDay,
  Oron Ann Clg 1% Condns DP=>MDB,     !- Name
          7,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       26.6,      !- Maximum Dry-Bulb Temperature {C}
        8.4,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       16.8,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     91757.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Oron_VD_CHE Annual Cooling (DP=>MDB) 2%, MDB=25.7C DP=16.5C
 SizingPeriod:DesignDay,
  Oron Ann Clg 2% Condns DP=>MDB,     !- Name
          7,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       25.7,      !- Maximum Dry-Bulb Temperature {C}
        8.4,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
       16.5,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     91757.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
