 ! The following Location and Design Day data are produced as possible from the weather source data.
 ! Wind Speeds follow the traditional values (6.7 m/s heating, 3.35 m/s cooling)
 ! No special attempts at re-creating or determining missing data parts (e.g. Wind speed or direction)
 ! are done.  Therefore, you should look at the data and fill in any incorrect values as you desire.
 
 ! SizingPeriod:DesignDay and other objects are formatted to meet criteria of EnergyPlus V7.1 and later
 
 ! Some users have indicated that they would like the actual day of week from the weather file to be
 ! used as the start day of week for a RunPeriod even though this is relatively meaningless for
 ! TMY data files as future months will not use their start days of week (simulations need to show
 ! continuity in day of week presentation).
 
 ! In an effort to accomodate, the following initial month days are included:
 ! Note JAN 1, 2014 is a Wednesday
 ! Note FEB 1, 2014 is a Saturday
 ! Note MAR 1, 2015 is a Sunday
 ! Note APR 1, 2017 is a Saturday
 ! Note MAY 1, 2018 is a Tuesday
 ! Note JUN 1, 2015 is a Monday
 ! Note JUL 1, 2016 is a Friday
 ! Note AUG 1, 2018 is a Wednesday
 ! Note SEP 1, 2014 is a Monday
 ! Note OCT 1, 2018 is a Monday
 ! Note NOV 1, 2018 is a Thursday
 ! Note DEC 1, 2014 is a Monday
 
 ! Since the RunPeriod object changes in V9.0, the following are RunPeriod objects in comments:
 ! Version before EnergyPlus release V9.0
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !Wednesday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes;            !- Use Weather File Snow Indicators
 
 ! Version EnergyPlus release V9.0+
 !RunPeriod,
 !Weather Data,    !- Name
 !  1,             !- Begin Month
 !  1,             !- Begin Day of Month
 !   ,             !- Begin Year
 !  12,            !- End Month
 !  31,            !- End Day of Month
 !   ,             !- End Year
 !Wednesday,      !- Day of Week for Start Day
 !  No,            !- Use Weather File Holidays and Special Days
 !  No,            !- Use Weather File Daylight Saving Period
 ! Yes,            !- Apply Weekend Holiday Rule
 ! Yes,            !- Use Weather File Rain Indicators
 ! Yes,            !- Use Weather File Snow Indicators
 ! ;               !- Treat Weather as Acual
 
 Site:Location,
  Gornergrat_VS_CHE Design_Conditions,     !- Location Name
      45.98,     !- Latitude {N+ S-}
       7.79,     !- Longitude {W- E+}
       1.00,     !- Time Zone Relative to GMT {GMT+/-}
    3129.30;     !- Elevation {m}
  
 ! The following Sizing Period objects for Extreme and Typical conditions are calculated
 ! from the extreme (if any) and typical conditions on the weather source data.
 ! The actual weeks that will be used will exist on the weather file.
  
 SizingPeriod:WeatherFileConditionType,
   Summer Extreme,
   SummerExtreme,
   SummerDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Summer Typical,
   SummerTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Extreme,
   WinterExtreme,
   WinterDesignDay,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Winter Typical,
   WinterTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Autumn Typical,
   AutumnTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 SizingPeriod:WeatherFileConditionType,
   Spring Typical,
   SpringTypical,
   Monday,
   Yes,                !- Use Weather File Daylight Saving Period
   Yes;                !- Use Weather File Rain and Snow Indicators
 


 ! Site:Precipitation. RoofIrrigation and Schedule:File objects for rainfall
 ! Data is built from rainfall data on the source data
  Site:Precipitation,  
    ScheduleAndDesignLevel,  !- Precipitation Model Type
    0.000,  !- Design Level for Total Annual Preciptation
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    0.000;  !- Average Total Annual Precipitation
 
  RoofIrrigation,  
    Schedule,  !- Irridation Model Type
    Rainfall Data from EPW file,    !- Precipitation Rates Schedule Name
    ;  !- Irrigation Maximum Saturation Threshold (default used)
 
  Schedule:File, 
    Rainfall Data from EPW file,  !- Name
    ,                       !- Schedule Type Limits Name
    CHE_VS_Gornergrat.067490_TMYx.2004-2018.rain,    !- File Name
    1,                       !- Column Number
    1,                       !- Rows to Skip at Top
    8760;    !- Number of Hours of Data


 ! No Design Conditions found for this Location
 ! Following definitions are created from weather source data
 ! Gornergrat_VS_CHE Annual Heating 99.6%, MaxDB=-19.1C
 SizingPeriod:DesignDay,
  Gornergrat Ann Htg 99.6% Condns DB,     !- Name
         12,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
      -19.1,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
      -19.1,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     68963.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Gornergrat_VS_CHE Annual Heating 99%, MaxDB=-18.6C
 SizingPeriod:DesignDay,
  Gornergrat Ann Htg 99% Condns DB,     !- Name
         12,      !- Month
         21,      !- Day of Month
  WinterDesignDay,!- Day Type
      -18.6,      !- Maximum Dry-Bulb Temperature {C}
        0.0,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Wetbulb,      !- Humidity Condition Type
      -18.6,      !- Wetbulb at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     68963.,      !- Barometric Pressure {Pa}
       6.71,!- [N/A] Wind Speed {m/s} traditional 6.71 m/s (15 mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       0.00;      !- Clearness {0.0 to 1.1}
 
 ! Gornergrat_VS_CHE Annual Cooling (DP=>MDB) .4%, MDB=12.8C DP=6.7C
 SizingPeriod:DesignDay,
  Gornergrat Ann Clg .4% Condns DP=>MDB,     !- Name
          8,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       12.8,      !- Maximum Dry-Bulb Temperature {C}
        5.6,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
        6.7,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     68963.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Gornergrat_VS_CHE Annual Cooling (DP=>MDB) 1%, MDB=12.3C DP=5.9C
 SizingPeriod:DesignDay,
  Gornergrat Ann Clg 1% Condns DP=>MDB,     !- Name
          8,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       12.3,      !- Maximum Dry-Bulb Temperature {C}
        5.6,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
        5.9,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     68963.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
 ! Gornergrat_VS_CHE Annual Cooling (DP=>MDB) 2%, MDB=11.7C DP=5.4C
 SizingPeriod:DesignDay,
  Gornergrat Ann Clg 2% Condns DP=>MDB,     !- Name
          8,      !- Month
         21,      !- Day of Month
  SummerDesignDay,!- Day Type
       11.7,      !- Maximum Dry-Bulb Temperature {C}
        5.6,      !- Daily Dry-Bulb Temperature Range {C}
 DefaultMultipliers, !- Dry-Bulb Temperature Range Modifier Type
           ,      !- Dry-Bulb Temperature Range Modifier Day Schedule Name
    Dewpoint,     !- Humidity Condition Type
        5.4,      !- Dewpoint at Maximum Dry-Bulb {C}
           ,      !- Humidity Indicating Day Schedule Name
           ,      !- Humidity Ratio at Maximum Dry-Bulb {kgWater/kgDryAir}
           ,      !- Enthalpy at Maximum Dry-Bulb {J/kg}
           ,      !- Daily Wet-Bulb Temperature Range {deltaC}
     68963.,      !- Barometric Pressure {Pa}
       3.35,   !- [N/A] Wind Speed {m/s} traditional 3.35 m/s (7mph)
          0,   !- [N/A] Wind Direction {Degrees; N=0, S=180}
         No,      !- Rain {Yes/No}
         No,      !- Snow on ground {Yes/No}
         No,      !- Daylight Savings Time Indicator
  ASHRAEClearSky, !- Solar Model Indicator
           ,      !- Beam Solar Day Schedule Name
           ,      !- Diffuse Solar Day Schedule Name
           ,      !- ASHRAE Clear Sky Optical Depth for Beam Irradiance (taub)
           ,      !- ASHRAE Clear Sky Optical Depth for Diffuse Irradiance (taud)
       1.00;      !- Clearness {0.0 to 1.1}
 
